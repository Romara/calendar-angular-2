# README #

### Prepearing ###

* Install [Node.js and npm](https://nodejs.org/en/download/) if they are not already on your machine.

**Verify that you are running at least node v4.x.x and npm 3.x.x by running node -v and npm -v in a terminal/console window. Older versions produce errors.**

### Server ###

Here is [my django project](https://bitbucket.org/Romara/django-server-for-myfirstangular2project) for this application   

### Contribution guidelines ###

* Fetch the code from this repository

```
#!terminal

cd ProjectName
```

* We install the packages listed in package.json using npm. Enter the following command in a terminal window  
(just in case i'm execute all comands as root).

```
#!terminal

sudo npm install
```

* The typings folder could not show up after npm install. If so, please install them manually.

```
#!terminal

sudo npm run typings install
```

* npm start - runs the compiler and a server at the same time, both in "watch mode"

```
#!terminal

sudo npm start
```

If there a lot of warrnings it's normal. If there is some error's it's very bad, if so report it to me.

### Important ###
All requests are sent to localhost:8080    
__Change port to your django-server port in event/event.service.ts, if it's not 8080.__  
Without server you will did not see any functionality, only calendar).
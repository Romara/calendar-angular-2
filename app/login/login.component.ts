import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { EventService } from '../event/event.service';


@Component({
  selector: 'login',
  templateUrl: 'app/login/login.component.html',
  styleUrls: ['app/login/login.component.css'],
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private eventService: EventService) {}

  error: string;
  status: string;

  login: string;
  password: string;

  loginF: string;
  passwordF: string;
  emailF:string;

  reg: any;

  ngOnInit(){
    this.error = '';
    this.reg = false;
    this.eventService.getProfile().then(res => this.check_login({'status': 200})).catch(err=>this.catchError(err))
  }

  submit(){
    this.eventService.login(this.login, this.password).then(response=>this.check_login(response)).catch(err => this.errorLogin(err));
  }

  register(){
    this.eventService.register(this.loginF, this.passwordF, this.emailF).then(response=>this.check_login(response)).catch(err => this.errorRegistration(err));
  }

  catchError(err){
    // Do nothing
  }

  check_login(res){
    if (res['status'] == 200){
      let link = ['Calendar'];
      this.router.navigate(link);
    }
    if (res['status'] == 201){
      this.status = 'Register successful';
      this.error = '';
      this.reg = !this.reg
    }
  }

  errorRegistration(error){
    this.error = error.json()['error'];
    this.status = '';
  }
  errorLogin(error){
    this.status = '';
    this.error = error.json()['error'];
  }

  some(){
    this.reg = true;
  }
}

import { Component } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import { AddEventComponent } from './addEvent/addEvent.component';
import { CalendarComponent } from './calendar/calendar.component';
import { LoginComponent } from './login/login.component'
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventService } from './event/event.service';

@Component({
  selector: 'app',
  template: `
    <router-outlet></router-outlet>
  `,
  providers: [
    EventService,
    ROUTER_PROVIDERS,
  ],
  directives: [
    ROUTER_DIRECTIVES,
    AddEventComponent
  ],
})

@RouteConfig([
  {
    path: '/calendar',
    name: 'Calendar',
    component: CalendarComponent,

  },
  {
    path: '/calendar/:id',
    name: 'EventDetail',
    component: EventDetailComponent
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginComponent,
    useAsDefault: true
  }
])

export class AppComponent {
  title = 'Calendar';
}

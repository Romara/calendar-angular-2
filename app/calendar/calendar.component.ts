import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { AddEventComponent } from '../addEvent/addEvent.component';
import { EditEventComponent } from '../editEvent/editEvent.component';
import { NotificationComponent } from '../notification/notification.component';
import { EventService } from '../event/event.service';
import { Event } from '../event/event';

export class CalendarDay {
  month_day: number;
  month_week: number;
  month_weekday: number;
  month_this: number;
  edit: number;
  events: DayEvent[];
}

export class DayEvent {
  event: Event;
  position: number;
  color: string;
  type: number; // 0-default 1-begin 2-end
  name: string;
}

@Component({
  selector: 'calendar',
  templateUrl: 'app/calendar/calendar.component.html',
  styleUrls: ['app/calendar/calendar.component.css'],
  directives: [AddEventComponent, EditEventComponent, NotificationComponent],
})

export class CalendarComponent implements OnInit, AfterViewInit {
  year: number; // Current year
  month: number; // Current month
  calendarWeek: CalendarDay[][]; // Calendar
  months: string[]; // Months title
  today: any; // today date

  // objects for creating events
  eventBeginDay: any;
  eventEndDay: any;
  mouseX: number;
  mouseY: number;
  mouseXcur: string;
  mouseYcur: string;

  // Hide'n'seek booleans
  edit: number;
  addEvent: number;
  editEvent: number;
  moveEvent: number;

  // Hide'n'seek events
  editObjEvent: any;
  moveObjEvent: any;

  // Objects support's event to move
  moved: any;
  dayMove: any;
  dayCount: number;

  // %number% px
  tableHeight: string;

  // weeks count
  trCount: number;

  // click on more
  showEvents: number;
  shownEvents: DayEvent[];
  showEventsDay: any;

  // Boolean for event refresh
  refresh: any;

  // my event's
  events: Event[];

  //user info
  username: string;

  constructor(
    private router: Router,
    private eventService: EventService) {
    }

  ngOnInit(){
    this.eventService.getProfile().then(name => this.username = name).catch(err=>this.catchError(err))
    this.today = new Date();
    this.year = this.today.getYear() + 1900;
    this.month = this.today.getMonth();
    this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.calendarWeek = [];
    this.fillCalendar();
    this.edit = 0;
    this.eventBeginDay = 0;
    this.eventEndDay = 0;
    this.addEvent = 0;
    this.events = [];
    this.getEvents();
    this.moveEvent = 0;
    this.showEvents = 0;
    this.moved = 0;
    this.refresh = 1;
  }

  catchError(error){
    if(error['status']==401||error['status']==403){
      let link = ['Login'];
      this.router.navigate(link);
    }
  }

  ngAfterViewInit(){
    this.getHeightCalendar();
  }

  // Get static color
   getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    // return color;
    return 'rgb(154, 156, 255)';
  }


  getEvents(){
    this.refresh = 0;
    this.eventService.loadEvents().then(events => this.events = events).then(()=>this.getHeightCalendar()).then(()=>this.fillEvents()).then(()=>this.refresh = 1);
  }


  fillEvents(){
    for(let week in this.calendarWeek){
      for(let day in this.calendarWeek[week]){
        this.calendarWeek[week][day].events = [];
      }
    }
    for(let event in this.events){
      let position = -1;
      let color = this.getRandomColor();
      let edit = 0;
      for(let week in this.calendarWeek){
        for(let day in this.calendarWeek[week]){
          let today;
          if (this.calendarWeek[week][day].month_this != 1){
            if (this.calendarWeek[week][day].month_day > 15){
              today = new Date(Date.UTC(this.year, this.month - 1, this.calendarWeek[week][day].month_day));
            }
            if (this.calendarWeek[week][day].month_day < 15){
              today = new Date(Date.UTC(this.year, this.month + 1, this.calendarWeek[week][day].month_day));
            }
          }
          else{
            today = new Date(Date.UTC(this.year, this.month, this.calendarWeek[week][day].month_day));
          }
          let buff = new Date(this.events[event].dateBegin);
          if(this.cmpDate(today, '==', buff)){
            for(let i = 0; i <= this.calendarWeek[week][day].events.length; i++){
              if(this.calendarWeek[week][day].events[i] == undefined){
                position = i;
                break;
              }
            }
            edit = 1;
          }
          if (edit){
            if(position != -1){
              let eeeeeevent = new DayEvent();
              buff = new Date(this.events[event].dateBegin);
              eeeeeevent.type = 0;
              if(this.cmpDate(today, '==', buff)){
                eeeeeevent.name = this.events[event].name;
                eeeeeevent.type += 1;
              }
              if(today.getDay() == 1){
                eeeeeevent.name = this.events[event].name;
              }
              buff = new Date(this.events[event].dateEnd);
              if(this.cmpDate(today, '==', buff)){
                eeeeeevent.type += 2;
              }
              eeeeeevent.event = this.events[event];
              eeeeeevent.position = position;
              eeeeeevent.color = color;
              this.calendarWeek[week][day].events[position] = eeeeeevent;
            }
            buff = new Date(this.events[event].dateEnd);
            if(this.cmpDate(today, '==', buff)){
              edit = 0;
            }
          }
        }
      }
    }
  }


  getEventWidth(){
    let a = document.getElementById("OuterSvgW").offsetWidth + 1;
    let b = a + 'px';
    return b;
  }


  fillCalendar(){
    let daysInThisMonth = this.daysInMonth(this.month);
    let daysInLastMonth = this.daysInMonth(this.month - 1);

    let daysInLastMonthLeft = new Date(Date.UTC(this.year, this.month, 1)).getDay() - 1;

    if (daysInLastMonthLeft == -1){
      daysInLastMonthLeft = 6;
    }
    let days = daysInLastMonthLeft + daysInThisMonth;
    let weeks = Math.ceil(days/7);

    let calendar = [];

    for (let i = 0; i < daysInLastMonthLeft; i++){
      calendar[i] = new CalendarDay();
      calendar[i].month_day = daysInLastMonth - (daysInLastMonthLeft - i - 1);
      calendar[i].month_week = 0;
      calendar[i].month_weekday = i%7;
      calendar[i].month_this = 0;
      calendar[i].events = [];
      if (
            (this.eventBeginDay <= new Date(Date.UTC(this.year, this.month - 1, calendar[i].month_day)))
            &&
            (this.eventEndDay >= new Date(Date.UTC(this.year, this.month - 1, calendar[i].month_day)))
          ){
        calendar[i].edit = 1;
      }
      else{
        calendar[i].edit = 0;
      }
    }
    for (let i = daysInLastMonthLeft; i < daysInThisMonth + daysInLastMonthLeft; i++){
      calendar[i] = new CalendarDay();
      calendar[i].month_day = i + 1 - daysInLastMonthLeft;
      calendar[i].month_week = i/7|0;
      calendar[i].month_weekday = i%7;
      calendar[i].month_this = 1;
      calendar[i].events = [];
      if (this.eventBeginDay <= new Date(Date.UTC(this.year, this.month, calendar[i].month_day))&&this.eventEndDay >= new Date(Date.UTC(this.year, this.month, calendar[i].month_day))){
        calendar[i].edit = 1;
      }
      else{
        calendar[i].edit = 0;
      }
    }
    for (let i = daysInThisMonth + daysInLastMonthLeft; (i)%7 != 0; i++){
      calendar[i] = new CalendarDay();
      calendar[i].month_day = i - daysInThisMonth - daysInLastMonthLeft + 1;
      calendar[i].month_week = i/7|0;
      calendar[i].month_weekday = i%7;
      calendar[i].month_this = 0;
      calendar[i].events = [];
      if (this.eventBeginDay <= new Date(Date.UTC(this.year, this.month + 1, calendar[i].month_day))&&this.eventEndDay >= new Date(Date.UTC(this.year, this.month + 1, calendar[i].month_day))){
        calendar[i].edit = 1;
      }
      else{
        calendar[i].edit = 0;
      }
    }

    for (let i = 0; i < weeks; i++){
      this.calendarWeek[i] = [];
      for (let j = 0; j < 7; j++){
        this.calendarWeek[i][j] = calendar[i*7 + j];
      }
    }

    for (let i = weeks; i < 6; i++){
      this.calendarWeek[i] = [];
      for (let j = 0; j < 7; j++){
        this.calendarWeek[i].pop();
      }
    }

    while(weeks != this.calendarWeek.length){
      this.calendarWeek.pop();
    }
    this.fillEvents();
  }

  daysInMonth(month: number) {
		return 33 - new Date(Date.UTC(this.year, month, 33)).getDate();
	}

  monthUp() {
    this.month += 1;
    if (this.month == 12){
      this.month = 0;
      this.year += 1;
    }
    this.fillCalendar();
  }

  monthDown() {
    this.month -= 1;
    if (this.month == -1){
      this.month = 11;
      this.year -= 1;
    }
    this.fillCalendar();
  }

  onScroll(event) {
    event = event || window.event;
    var delta = event.deltaY || event.detail || event.wheelDelta;
    if (delta > 0){
      this.monthUp();
    }
    if (delta < 0){
      this.monthDown();
    }
  }

  onClick(day: number, thisMonth: number){
    this.addEvent = 0;
    this.showEvents = 0;
    let month_b;
    month_b = this.month;
    if (!thisMonth){
      if (day > 15) {
        month_b -= 1;
      }else{
        month_b += 1;
      }
    }
    this.eventBeginDay = new Date(Date.UTC(this.year, month_b, day));
    this.eventEndDay = 0;
    this.edit = 1;
    if(this.editEvent){
      this.getEvents();
    }
    this.editEvent = 0;
  }

  onMouseUp(event, day: number, thisMonth: number){
    if(this.editEvent){
      this.getEvents();
    }
    this.editEvent = 0;
    let month_b;
    month_b = this.month;
    if (!thisMonth){
      if (day > 15) {
        month_b -= 1;
      }else{
        month_b += 1;
      }
    }
    if(this.edit){
      if(this.eventBeginDay == 0){
        return;
      }
      this.eventEndDay = new Date(Date.UTC(this.year, month_b, day));
      if (this.eventEndDay < this.eventBeginDay){
        let buff = this.eventEndDay;
        this.eventEndDay = this.eventBeginDay;
        this.eventBeginDay = buff;
      }
      this.renderCalendar();
      this.edit = 0;
      this.addEvent = 1;
      this.mouseX = event.clientX;
      this.mouseY = event.clientY;
    }
    if(this.moveEvent){
      this.moveEvent = 0;
      if(!this.moved){
        return;
      }
      this.moved = 0;
      this.moveObjEvent.event.dateBegin = new Date(Date.UTC(this.year, month_b, day));
      this.moveObjEvent.event.dateEnd = new Date(Date.UTC(this.year, month_b, day + this.dayCount - 1));
      this.eventService.saveCustomEvent(this.moveObjEvent.event).then(()=>this.getEvents());
      this.fresh();
    }
  }

  onMouseMove(event, day, thisMonth){
    let month_b;
    month_b = this.month;
    if (!thisMonth){
      if (day > 15) {
        month_b -= 1;
      }else{
        month_b += 1;
      }
    }
    if(this.edit){
      if(this.eventEndDay != new Date(Date.UTC(this.year, month_b, day))){
        this.eventEndDay = new Date(Date.UTC(this.year, month_b, day));
      }
    }
    if(this.moveEvent){
      this.dayMove = new Date(Date.UTC(this.year, month_b, day))
      this.moved = 1;
    }
    this.renderCalendar();
  }

  mouseMove(event){
    let a = event.clientX - 10;
    this.mouseXcur = a + 'px';
    a = event.clientY + 10;
    this.mouseYcur = a + 'px';
  }

  // This function called by other components
  finished(num){
    if(this.editEvent){
      this.getEvents();
    }
    this.eventBeginDay = 0;
    this.eventEndDay = 0;
    this.addEvent = 0;
    this.editEvent = 0;
    if (num != 2){
      this.getEvents();
    }
    if(num == 3){
      let link = ['EventDetail', { id: this.editObjEvent.id }];
      this.router.navigate(link);
    }
    this.fresh();
  }

  openEvent(num){
    console.log(num)
    let link = ['EventDetail', { id: num }];
    this.router.navigate(link);
  }

  onMouseLeave(month_day, thisMonth){
    this.edit = 0;
  }

  // Just render calendar without fill it
  // use it if some event moved or changed
  // much faster then fillcalendar()
  renderCalendar(){
    let eventBeginDay = this.eventBeginDay;
    let eventEndDay = this.eventEndDay;
    if (this.eventBeginDay > this.eventEndDay){
      eventBeginDay = this.eventEndDay;
      eventEndDay = this.eventBeginDay;
    }
    for (let i = 0; i < this.calendarWeek.length; i++){
      for (let j = 0; j < this.calendarWeek[i].length; j++){
        if(this.calendarWeek[i][j].month_this){
          if ((eventBeginDay <= new Date(Date.UTC(this.year, this.month, this.calendarWeek[i][j].month_day)))&&(eventEndDay >= new Date(Date.UTC(this.year, this.month, this.calendarWeek[i][j].month_day)))){
            this.calendarWeek[i][j].edit = 1;
          }
          else{
            this.calendarWeek[i][j].edit = 0;
          }
        }
        else{
          if (this.calendarWeek[i][j].month_day > 15){
            if ((eventBeginDay <= new Date(Date.UTC(this.year, this.month - 1, this.calendarWeek[i][j].month_day)))&&(eventEndDay >= new Date(Date.UTC(this.year, this.month - 1, this.calendarWeek[i][j].month_day)))){
              this.calendarWeek[i][j].edit = 1;
            }
            else{
              this.calendarWeek[i][j].edit = 0;
            }
          }
          else{
            if ((eventBeginDay <= new Date(Date.UTC(this.year, this.month + 1, this.calendarWeek[i][j].month_day)))&&(eventEndDay >= new Date(Date.UTC(this.year, this.month + 1, this.calendarWeek[i][j].month_day)))){
              this.calendarWeek[i][j].edit = 1;
            }
            else{
              this.calendarWeek[i][j].edit = 0;
            }
          }
        }
      }
    }
    if(this.moveEvent){
      let count = this.dayCount;
      let push = false;
      for (let i = 0; i < this.calendarWeek.length; i++){
        for (let j = 0; j < this.calendarWeek[i].length; j++){
          if(count == 0){
            break;
          }
          let month_b;
          month_b = this.month;
          if (!this.calendarWeek[i][j].month_this){
            if (this.calendarWeek[i][j].month_day > 15) {
              month_b -= 1;
            }else{
              month_b += 1;
            }
          }
          if(this.cmpDate(this.dayMove, '==', new Date(Date.UTC(this.year, month_b, this.calendarWeek[i][j].month_day)))){
            push = true;
          }
          if(push){
            this.calendarWeek[i][j].edit = 1;
            count -= 1;
          }
        }
      }
    }
  }

  getTop(week){
    try{
      let proc = 100/this.calendarWeek.length*week[0].month_week;
      return proc + '%';
    }
    catch(er){
      return '1%';
    }
  }

  getHeight(week){
    try{
      let proc = 100/this.calendarWeek.length;
      return proc + '%';
    }
    catch(er){
      return '1%';
    }
  }

  // Used by ngFor
  getNumber(num){
    return new Array(num);
  }

  getHeightCalendar(){
    let tableHeight = window.outerHeight - 200;
    this.tableHeight = tableHeight + 'px';

    let cellHeight = document.getElementById("OuterSvgH").offsetHeight - 18;
    cellHeight = cellHeight/24;
    this.trCount = Math.floor(cellHeight);
  }

  // Refresh
  fresh(){
    this.eventBeginDay = 0;
    this.eventEndDay = 0;
    this.renderCalendar();
    this.addEvent = 0;
    // if(this.editEvent){
      this.getEvents();
    // }
    this.editEvent = 0;
    this.edit = 0;
    this.moveEvent = 0;
    this.showEvents = 0;
  }

  cmpEvent(event, day){
    let a = this.cmpDate(new Date(event.dateBegin), '<=', new Date(Date.UTC(this.year, this.month, day.month_day)));
    let b = this.cmpDate(new Date(event.dateEnd), '>=', new Date(Date.UTC(this.year, this.month, day.month_day)));
    return a&&b;
  }

  cmpDate(date1, operator, date2){
    if (operator == '=='||operator == '<='||operator == '>='){
      let a = date1.getYear() == date2.getYear()
      let b = date1.getMonth() == date2.getMonth()
      let c = date1.getDate() == date2.getDate()
      if(a&&b&&c){
        return true;
      }
    }
    if (operator == '<'||operator == '<='){
      let a = date1.getYear() < date2.getYear()
      if (!a){
        let b = date1.getMonth() < date2.getMonth()
        if (!b){
          let c = date1.getDate() < date2.getDate()
          return c;
        }
        return b;
      }
      return a;
    }
    if (operator == '>'||operator == '>='){
      let a = date1.getYear() > date2.getYear()
      if (!a){
        let b = date1.getMonth() > date2.getMonth()
        if (!b){
          let c = date1.getDate() > date2.getDate()
          return c;
        }
        return b;
      }
      return a;
    }
    return false;
  }

  // Render event.name only 1 time on dateBegin
  eventName(event, day){
    let a = new Date(event.dateBegin);
    return this.cmpDate(a, '==', new Date(Date.UTC(this.year, this.month, day.month_day)));
  }

  // event is $event
  // eventObj is obj type Event
  onSelect(event, eventObj){
    this.fresh();
    this.editObjEvent = eventObj.event;
    this.mouseX = event.clientX;
    this.mouseY = event.clientY;
    this.editEvent = 1;
  }

  onDoubleClickEvent(event){
    let link = ['EventDetail', { id: event.event.id }];
    this.router.navigate(link);
  }

  pickEvent(eventObj, day){
    if(this.editEvent){
      this.getEvents();
    }
    this.editEvent = 0;
    this.addEvent = 0;
    this.moveEvent = 1;
    this.moveObjEvent = eventObj;
    var e = new Date(this.moveObjEvent.event.dateEnd);
    var b = new Date(this.moveObjEvent.event.dateBegin)
    this.dayCount = Math.floor((e.getTime() - b.getTime()) / 24 / 60 / 60 / 1000) + 1;
  }

  // click on more
  showAllEvents(day){
    this.showEvents = 1;
    this.shownEvents = day.events;
    this.showEventsDay = day;
  }

  eventSTOP(event){
    event.preventDefault();
    event.stopPropagation();
  }

  logout(){
    this.eventService.logout();
    let link = ['Login'];
    this.router.navigate(link);
  }
}

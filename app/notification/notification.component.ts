import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EventService } from '../event/event.service';
import { Event } from '../event/event'

@Component({
  selector: 'notification',
  templateUrl: 'app/notification/notification.component.html',
})

export class NotificationComponent implements OnInit  {

constructor(private eventService: EventService){ }

  allEvents: any[];

  today: any;

  tasks: Event[];
  events: Event[];

  @Output() openEvent = new EventEmitter();

  ngOnInit(){
    this.today = new Date();
    this.allEvents = [];
    this.tasks = [];
    this.events = [];
    this.eventService.loadEvents().then(events=>this.allEvents=events).then(()=>this.sort())
  }

  open(id){
    this.openEvent.emit(id);
  }

  eventSTOP(event){
    event.preventDefault();
    event.stopPropagation();
  }

  getMaxHeight(){
    let height = window.innerHeight - 130;
    return height + 'px';
  }

  getDaysLeft(date){
    let date1 = new Date(date);
    let date2 = new Date();
    let dayCount = Math.floor((date1.getTime() - date2.getTime()) / 24 / 60 / 60 / 1000);
    return dayCount;
  }

  getDeadline(date){
    let date1 = new Date(date);
    let date2 = new Date();
    let dayCount = Math.floor((date1.getTime() - date2.getTime()) / 24 / 60 / 60 / 1000);
    if (dayCount < -1){
      return 'overdue';
    }
    if (dayCount == -1){
      return 'today';
    }
    return dayCount + ' days';
  }

  sort(){
    for (let event in this.allEvents){
      if (this.allEvents[event].eventType!=1){
        this.tasks.push(this.allEvents[event]);
      }else{
        this.events.push(this.allEvents[event]);
      }
    }
  }

  cmpDateWithToday(operator, date){
    let date1 = new Date(date);
    return this.cmpDate(this.today, operator, date1);
  }

  cmpDate(date1, operator, date2){
    if (operator == '=='||operator == '<='||operator == '>='){
      let a = date1.getYear() == date2.getYear()
      let b = date1.getMonth() == date2.getMonth()
      let c = date1.getDate() == date2.getDate()
      if(a&&b&&c){
        return true;
      }
    }
    if (operator == '<'||operator == '<='){
      let a = date1.getYear() < date2.getYear()
      if (!a){
        let b = date1.getMonth() < date2.getMonth()
        if (!b){
          let c = date1.getDate() < date2.getDate()
          return c;
        }
        return b;
      }
      return a;
    }
    if (operator == '>'||operator == '>='){
      let a = date1.getYear() > date2.getYear()
      if (!a){
        let b = date1.getMonth() > date2.getMonth()
        if (!b){
          let c = date1.getDate() > date2.getDate()
          return c;
        }
        return b;
      }
      return a;
    }
    return false;
  }
}

export class Event {
  id: number;
  name: string;
  description: string;
  dateSet: any; // Current date
  dateBegin: any; // Begin task date
  dateEnd: any;  // Deadline
  dateEnded: any; // Complited task     --Task only
  status: number; // Complited or not   --Task only
  type: number; // Task-1 or Event-0
}

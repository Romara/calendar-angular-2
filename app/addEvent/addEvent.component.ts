import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EventService } from '../event/event.service';


@Component({
  selector: 'addEvent',
  templateUrl: 'app/addEvent/addEvent.component.html',
})


  //onInit gets dateBegin and dateEnd
export class AddEventComponent implements OnInit  {
  type: number;
  hidden: number;
  taskName: string;

  @Input()
  dateBegin: any;
  @Input()
  dateEnd: any;
  @Input()
  xPos: any;
  @Input()
  yPos: any;

  @Output() finished = new EventEmitter();

  constructor(private eventService: EventService){ }

  ngOnInit(){
    this.hidden = 0;
    this.type = 0;
    this.xPos -= 200;
    if(this.xPos < 0){
      this.xPos = 0;
    }else{
      if (this.xPos + 400 > window.innerWidth){
        this.xPos = window.innerWidth - 400;
      }
    }
    this.xPos = this.xPos + 'px';
    this.yPos -= 200;
    if (this.yPos < 0){
      this.yPos += 200;
    }
    this.yPos = this.yPos + 'px';
  }

  back(){
    this.hidden = 1;
    this.finished.emit(2);
  }

  save(){
    this.eventService.saveEvent(this.taskName, this.dateBegin, this.dateEnd,  this.type).then(()=>this.finish());
    this.hidden = 1;
  }

  finish(){
    this.finished.emit(this.hidden);
  }
}

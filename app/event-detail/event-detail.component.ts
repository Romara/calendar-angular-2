import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';
import { EventService } from '../event/event.service';
import { Event } from '../event/event'

@Component({
  selector: 'event-detail',
  templateUrl: 'app/event-detail/event-detail.component.html',
})


export class EventDetailComponent implements OnInit  {
  event: Event;

constructor(private routeParams: RouteParams,
              private eventService: EventService,
              private router: Router){ }


  ngOnInit(){
    let id = +this.routeParams.get('id');
    this.eventService.getEvent(id).then(event => this.event = event);
  }

  changeStatus(){
    if(this.event.status){
      this.event.status = 0;
    }
    else{
      this.event.status = 1;
    }
    if(this.event.status){
      this.event.dateEnded = new Date();
    }
    else{
      this.event.dateEnded = "";
    }
  }

  delete(){
    this.eventService.deleteEvent(this.event.id).then(()=>this.goBack());
  }

  save(){
    this.eventService.saveCustomEvent(this.event).then(()=>this.goBack());
  }

  goBack(){
    let link = ['Calendar', {}];
    this.router.navigate(link);
  }
}

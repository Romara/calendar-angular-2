import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EventService } from '../event/event.service';


@Component({
  selector: 'editEvent',
  templateUrl: 'app/editEvent/editEvent.component.html',
})


export class EditEventComponent implements OnInit  {
  hidden: number;

  @Input()
  event: any;
  @Input()
  xPos: any;
  @Input()
  yPos: any;

  @Output() finished = new EventEmitter();

  constructor(private eventService: EventService){ }

  ngOnInit(){
    this.hidden = 0;
    this.xPos -= 200;
    if(this.xPos < 0){
      this.xPos = 0;
    }else{
      if (this.xPos + 400 > window.innerWidth){
        this.xPos = window.innerWidth - 400;
      }
    }
    this.xPos = this.xPos + 'px';
    this.yPos -= 300;
    if (this.yPos < 0){
      this.yPos += 300;
    }
    if(this.event.eventType==1){
      this.yPos += 80;
    }
    this.yPos = this.yPos + 'px';
  }

  changeStatus(){
    if(this.event.status){
      this.event.status = 0;
      this.event.dateEnded = null;
    }
    else{
      this.event.status = 1;
      this.event.dateEnded = new Date();
    }
    this.eventService.saveCustomEvent(this.event);
  }

  back(){
    this.hidden = 1;
    this.finished.emit(2);
  }

  edit(){
    this.hidden = 1;
    this.finished.emit(3);
  }

  finish(){
    this.finished.emit(this.hidden);
  }
}

import { Event } from '../event/event';
import { Injectable } from '@angular/core';
import { Jsonp,Headers, Http, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class EventService {
  apiUrl = 'http://192.168.13.112:8080/api/';

  constructor(private http: Http) {
    let _build = (<any> http)._backend._browserXHR.build;
      (<any> http)._backend._browserXHR.build = () => {
          let _xhr =  _build();
          _xhr.withCredentials = true;
          return _xhr;
      };
    }

  loadEvents(): Promise<Event[]> {
    let params = new URLSearchParams();
    params.set('format', 'json');
    return this.http.get(this.apiUrl + 'events/', { search: params }).toPromise().then(response => response.json());
  }

  getEvent(id: number){
    let params = new URLSearchParams();
    params.set('format', 'json');
    return this.http.get(this.apiUrl + 'event/' + id, { search: params }).toPromise().then(response => response.json());
  }

  deleteEvent(id: number){
    return this.http.delete(this.apiUrl + 'event/' + id).toPromise();
  }

  deleteEvents(){
    return this.http.delete(this.apiUrl + 'events/').toPromise();
  }

  // Create new event
  saveEvent(name: string, dateBegin: any, dateEnd: any,  type: number){
    let body = JSON.stringify({'name':name, 'dateBegin':dateBegin, 'dateEnd':dateEnd, 'type':type})
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.apiUrl + 'events/', body, options).toPromise().then(response => response.json());
  }

  // Save current event
  saveCustomEvent(event){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.apiUrl + 'event/' + event.id, JSON.stringify(event), {headers: headers}).toPromise().then(response => response.json());
  }

  result:any;

  login(login:string, password:string){
    let body = JSON.stringify({'username':login, 'password':password})
    let headers = new Headers({ 'Content-Type': 'application/json'
    // 'withCredentials': true
  });
    let options = new RequestOptions({ headers: headers,
    // 'withCredentials': true
   });
    return this.http.post(this.apiUrl + 'authorize/', body, options).toPromise();
  }

  logout(){
    return this.http.delete(this.apiUrl + 'authorize/').toPromise();
  }

  register(username:string, password:string, email:string){
    let body = JSON.stringify({'username':username, 'password':password, 'email':email})
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.apiUrl + 'register/', body, options).toPromise();
  }

  getProfile(){
    return this.http.get(this.apiUrl + 'profile/').toPromise().then(response => response.json());
  }
}

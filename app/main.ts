import { bootstrap }    from '@angular/platform-browser-dynamic';
import { enableProdMode, Injectable, provide } from '@angular/core';
import { AppComponent } from './app.component';

import { Http, XHRBackend, HTTP_PROVIDERS, BrowserXhr, Headers, BaseRequestOptions, RequestOptions } from '@angular/http';

enableProdMode();

bootstrap(AppComponent, [HTTP_PROVIDERS])
